package com.example.andreacornini.jekocamera;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.Typeface;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CameraManager;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Process;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.SparseArray;
import android.util.TypedValue;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.vision.CameraSource;
import com.google.android.gms.vision.Detector;
import com.google.android.gms.vision.barcode.Barcode;
import com.google.android.gms.vision.barcode.BarcodeDetector;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;

import cz.msebera.android.httpclient.Header;

import static com.example.andreacornini.jekocamera.R.layout.activity_main;

public class MainActivity extends AppCompatActivity {
    private CameraManager cameraManager;
    private BarcodeDetector barcodeDetector;
    public SharedPreferences sharedPref;
    public static String currentQRCode = null;
    public static final String MY_PREFERENCES = "JekoCamera";
    //public static final String TEXT_DATA_KEY = "path";
    public static final String TEXT_DATA_KEY2 = "textData2";
    public static final String TEXT_DATA_KEY3 = "textData3";
    //public static final String TEXT_DATA_KEY4 = "code";
    public  static final int RequestPermissionCode  = 1;
    public static String currentPhoto = null;
    public Bitmap bitmap = null;

    private CameraSource cameraSource;
    private SurfaceView cameraView;
    private TextView txtIdCode;
    private TextView txtPhotos;
    private Button btnConferma;
    private Button btnAnnulla;
    private Button btnScattaFoto;
    ProgressDialog progressDialog;
    public String code = null;
    public static String path = null;
    public AlertDialog.Builder alertDialogBuilder;
    public ProgressDialog dialog = null;
    public String upLoadServerUri = null;
    public boolean transferEnCourse = false;
    Intent intent;
    String mCurrentPhotoPath;
    Uri outPutFileUri;
    String ImageUploadPathOnSever ="https://jekoapi.jekoweb.it/apijeko/invioJekoCamera";
    @Override
    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        setContentView(activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("JekoCamera");
        sharedPref = getSharedPreferences(MY_PREFERENCES, Context.MODE_PRIVATE);
        //EnableRuntimePermissionToAccessCamera();
        txtIdCode = (TextView) this.findViewById(R.id.txtIdCode);
        txtPhotos = (TextView) this.findViewById(R.id.txtPhotos);
        txtIdCode.setText("");
        txtPhotos.setText("Nessuna foto salvata");
        btnConferma = (Button) this.findViewById(R.id.btnConferma);
        btnAnnulla = (Button) this.findViewById(R.id.btnAnnulla);
        btnScattaFoto = (Button) this.findViewById(R.id.btnScattaFoto);
        cameraView = (SurfaceView) findViewById(R.id.sfvCamera);
        barcodeDetector = new BarcodeDetector.Builder(this)
                .setBarcodeFormats(Barcode.QR_CODE)
                .build();
        cameraSource = new CameraSource.Builder(this, barcodeDetector)
                .setRequestedPreviewSize(1600, 1024)
                .setAutoFocusEnabled(true)
                .build();
        barcodeDetector.setProcessor(new Detector.Processor<Barcode>() {
            @Override
            public void release() {
            }

            @Override
            public void receiveDetections(Detector.Detections<Barcode> detections) {
                final SparseArray<Barcode> barcodes = detections.getDetectedItems();

                if (barcodes.size() != 0) {
                    txtIdCode.post(new Runnable() {    // Use the post method of the TextView
                        public void run() {
                            txtIdCode.setText(barcodes.valueAt(0).displayValue);
                            code = txtIdCode.getText().toString();
                            currentQRCode = txtIdCode.getText().toString();
                        }
                    });
                }
            }
        });

        cameraView.getHolder().addCallback(new SurfaceHolder.Callback() {
            @Override
            public void surfaceCreated(SurfaceHolder holder) {
                if (ContextCompat.checkSelfPermission(MainActivity.this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                    //richiede il permesso
                    ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.CAMERA}, 1);
                } else {
                    //ce l'ho già, quindi posso fare quello che devo fare
                    try {
                        start_camera();
                        cameraSource.start(cameraView.getHolder());
                    } catch (CameraAccessException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {

            }

            @Override
            public void surfaceDestroyed(SurfaceHolder holder) {
                cameraSource.stop();
            }
        });
        btnConferma.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                try {
                    final File directory = new File(path);
                    if (!txtIdCode.getText().equals("") && directory.list().length > 0) {
                        String[] separated = txtIdCode.getText().toString().split("|");
                        String azz = "";
                        for (int i = 0; i < separated.length; i++) {
                            azz += separated[i];
                        }
                        final String idSM = azz.substring(0, azz.indexOf('|'));
                        final String idRichiesta = azz.substring(azz.lastIndexOf('|'));
                        String idRichiestaFinal = idRichiesta.substring(1);
                        int totImmagini = directory.listFiles().length;
                        ImageUploadToServerFunction(idSM, idRichiestaFinal, directory);
                        //invokeWS("https://www.jekoweb.it/apiJeko/invioJekoCamera", "QR Code e foto inviate con successo", "Errore durante l'invio", idSM, idRichiesta, Main2Activity.path + "/" + fileEntry.getName(), fileEntry.getName());
                    } else
                        Toast.makeText(MainActivity.this, "Attenzione: accertarsi di aver scannerizzato un QR Code e di aver salvato almeno una foto del prodotto prima di inviare!", Toast.LENGTH_LONG).show();
                    txtPhotos.setText("Nessuna foto salvata");
                    txtIdCode.setText("");
                }
                catch (Exception ex){
                    Toast.makeText(MainActivity.this, "Attenzione: si è verificato un errore durante l'invio!", Toast.LENGTH_LONG).show();
                }
            }
        });
        btnAnnulla.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                txtIdCode.setText("");
                try {
                    File directory = new File(path);
                    try {
                        delete(directory);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                txtPhotos.setText("Nessuna foto salvata");
            }
        });
        btnScattaFoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!txtIdCode.getText().equals("")) {
                    intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                    // Ensure that there's a camera activity to handle the intent
                    if (intent.resolveActivity(getPackageManager()) != null) {
                        // Create the File where the photo should go
                        File photoFile = null;
                        try {
                            photoFile = createImageFile();
                        } catch (IOException ex) {
                            // Error occurred while creating the File

                        }
                        // Continue only if the File was successfully created
                        if (photoFile != null) {
                            Uri photoURI = FileProvider.getUriForFile(MainActivity.this,
                                    "com.example.android.fileprovider",
                                    photoFile);
                            intent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                            outPutFileUri = photoURI;
                            startActivityForResult(intent, 7);
                        }
                    }
                }
                else{
                    Toast.makeText(getApplicationContext(), "Prima di fotografare il prodotto leggere il QR Code!", Toast.LENGTH_LONG).show();
                }
            }
        });
        alertDialogBuilder = new AlertDialog.Builder(MainActivity.this);
        txtIdCode.setText(currentQRCode);
        if(currentPhoto != null)
            txtPhotos.setText(currentPhoto);
        else
            txtPhotos.setText("Nessuna foto salvata");
        File directory = new File("/storage/emulated/0/Android/data/com.example.andreacornini.jekocamera/files/Pictures");
        deleteDir(directory);
    }

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        code = MainActivity.currentQRCode;
        //code = sharedPref.getString(TEXT_DATA_KEY2, null);
        String[] separated = code.split("|");
        String azz = "";
        for(int i = 0; i < separated.length; i++){
            azz += separated[i];
        }
        String idSM = azz.substring(0, azz.indexOf('|'));
        final String idRichiesta = azz.substring(azz.lastIndexOf('|'));
        String idRichiestaFinal = idRichiesta.substring(1);
        String photoFileInt = idRichiestaFinal + "_" + timeStamp + ".jpg";
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = new File(storageDir, photoFileInt);

        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = image.getAbsolutePath();
        path = storageDir.getAbsolutePath();
        return image;
    }

    // Star activity for result method to Set captured image on image view after click.
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);
        File directory = new File(path);
        File[] files = directory.listFiles();
        int numberOfFiles = files.length;
        if(numberOfFiles == 0)
            currentPhoto = "Nessuna foto salvata";
        if(numberOfFiles == 1)
            currentPhoto = numberOfFiles + " foto salvata";
        else
            currentPhoto = numberOfFiles + " foto salvate";
        if (requestCode == 7 && resultCode == RESULT_OK) { // && data != null && data.getData() != null
            try {
                // Adding captured image in bitmap.
                Bundle extras = data.getExtras();
                Bitmap pippo = (Bitmap) extras.get("data");
                bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), outPutFileUri);
                // adding captured image in imageview.
            } catch (Exception e) {

                e.printStackTrace();
            }
        }
        txtPhotos.setText(currentPhoto);
    }

    // Requesting runtime permission to access camera.
    public void EnableRuntimePermissionToAccessCamera(){

        if (ActivityCompat.shouldShowRequestPermissionRationale(MainActivity.this,
                Manifest.permission.CAMERA))
        {

            // Printing toast message after enabling runtime permission.
            //Toast.makeText(Main2Activity.this,"CAMERA permission allows us to Access CAMERA app", Toast.LENGTH_LONG).show();

        } else {

            ActivityCompat.requestPermissions(MainActivity.this,new String[]{Manifest.permission.CAMERA}, RequestPermissionCode);

        }
        if (ActivityCompat.shouldShowRequestPermissionRationale(MainActivity.this,
                Manifest.permission.READ_EXTERNAL_STORAGE))
        {

            // Printing toast message after enabling runtime permission.
            //Toast.makeText(Main2Activity.this,"READ_EXTERNAL_STORAGE permission enabled", Toast.LENGTH_LONG).show();

        } else {

            ActivityCompat.requestPermissions(MainActivity.this,new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, RequestPermissionCode);

        }
        if (ActivityCompat.shouldShowRequestPermissionRationale(MainActivity.this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE))
        {

            // Printing toast message after enabling runtime permission.
            //Toast.makeText(Main2Activity.this,"WRITE_EXTERNAL_STORAGE permission enabled", Toast.LENGTH_LONG).show();

        } else {

            ActivityCompat.requestPermissions(MainActivity.this,new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, RequestPermissionCode);

        }
    }

    public void invokeWS(String uri, final String rispOk, final String rispErr, String idSM, String idRichiesta, final String path, String imageName){
        String username = sharedPref.getString(TEXT_DATA_KEY2, null);
        String password = sharedPref.getString(TEXT_DATA_KEY3, null);
        //StringEntity entity = null;
        AsyncHttpClient client = new AsyncHttpClient();
        client.setBasicAuth(username,password);


        String image = getImageBase64(path);
        image.replaceAll("\n","");
        RequestParams params = new RequestParams();
        params.put("idSM",idSM);
        params.put("idRichiesta",idRichiesta);
        params.put("image",image);
        params.put("imageName",imageName);
        client.post(uri, params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                try {
                    JSONObject obj = response;
                    if(obj.getBoolean("Valid")){
                        alertDialogBuilder
                                .setMessage(rispOk)
                                .setCancelable(false)
                                .setPositiveButton("OK",new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,int id) {
                                        dialog.cancel();
                                    }
                                });
                        AlertDialog alertDialog = alertDialogBuilder.create();
                        alertDialog.show();
                        TextView textView = (TextView) alertDialog.findViewById(android.R.id.message);
                        textView.setTypeface(Typeface.MONOSPACE);
                        textView.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 18);
                        txtIdCode.setText("");
                        File fileDelete = new File(path);
                        fileDelete.delete();
                        if (progressDialog.getProgress() == progressDialog.getMax()) {
                            progressDialog.dismiss();
                        }
                        else {
                            progressDialog.incrementProgressBy(1);
                        }
                    }
                    else{
                        alertDialogBuilder
                                .setMessage(rispErr)
                                .setCancelable(false)
                                .setPositiveButton("OK",new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,int id) {
                                        dialog.cancel();
                                    }
                                });
                        AlertDialog alertDialog = alertDialogBuilder.create();
                        alertDialog.show();
                        TextView textView = (TextView) alertDialog.findViewById(android.R.id.message);
                        textView.setTypeface(Typeface.MONOSPACE);
                        textView.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 18);
                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    alertDialogBuilder
                            .setMessage("Error Occured [Server's JSON response might be invalid]!")
                            .setCancelable(false)
                            .setPositiveButton("OK",new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog,int id) {
                                    dialog.cancel();
                                }
                            });
                    AlertDialog alertDialog = alertDialogBuilder.create();
                    alertDialog.show();
                    TextView textView = (TextView) alertDialog.findViewById(android.R.id.message);
                    textView.setTypeface(Typeface.MONOSPACE);
                    textView.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 18);
                    e.printStackTrace();
                }
            }
            @Override
            public void onFailure(int statusCode, Header[] headers, String pippo, Throwable throwable) {
                alertDialogBuilder
                        .setMessage("Error: " + throwable.getMessage())
                        .setCancelable(false)
                        .setPositiveButton("OK",new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int id) {
                                dialog.cancel();
                            }
                        });
                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();
                TextView textView = (TextView) alertDialog.findViewById(android.R.id.message);
                textView.setTypeface(Typeface.MONOSPACE);
                textView.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 18);
            }
        });
    }


    private static Bitmap resize(Bitmap image, int maxWidth, int maxHeight) {
        if (maxHeight > 0 && maxWidth > 0) {
            int width = image.getWidth();
            int height = image.getHeight();

            float ratioBitmap = (float) width / (float) height;
            float ratioMax = (float) maxWidth / (float) maxHeight;

            int finalWidth = maxWidth;
            int finalHeight = maxHeight;
            if (ratioMax > 1) {
                finalWidth = (int) ((float)maxHeight * ratioBitmap);
            } else {
                finalHeight = (int) ((float)maxWidth / ratioBitmap);
            }
            image = Bitmap.createScaledBitmap(image, finalWidth, finalHeight, true);
            return image;
        } else {
            return image;
        }
    }
    //Converting Selected Image to Base64Encode String
    private String getImageBase64(String path) {
        Bitmap myImg = null;
        try {
            myImg = BitmapFactory.decodeFile(path);
        } catch (Exception e) {
            e.printStackTrace();
        }
        ExifInterface oldExif = null;
        try {
            oldExif = new ExifInterface(path);
        } catch (IOException e) {
            e.printStackTrace();
        }
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        // Must compress the Image to reduce image size to make upload easy
        Bitmap photo = resize(rotateBitmap(path,myImg), 1280, 960);
        photo.compress(Bitmap.CompressFormat.JPEG, 100, stream);
        byte[] byte_arr = stream.toByteArray();
        // Encode Image to String
        return  android.util.Base64.encodeToString(byte_arr, 0);
    }

    public static Bitmap rotateBitmap(String src, Bitmap bitmap) {
        try {
            int orientation = getExifOrientation(src);

            if (orientation == 1) {
                return bitmap;
            }

            Matrix matrix = new Matrix();
            switch (orientation) {
                case 2:
                    matrix.setScale(-1, 1);
                    break;
                case 3:
                    matrix.setRotate(180);
                    break;
                case 4:
                    matrix.setRotate(180);
                    matrix.postScale(-1, 1);
                    break;
                case 5:
                    matrix.setRotate(90);
                    matrix.postScale(-1, 1);
                    break;
                case 6:
                    matrix.setRotate(90);
                    break;
                case 7:
                    matrix.setRotate(-90);
                    matrix.postScale(-1, 1);
                    break;
                case 8:
                    matrix.setRotate(-90);
                    break;
                default:
                    return bitmap;
            }

            try {
                Bitmap oriented = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
                bitmap.recycle();
                return oriented;
            } catch (OutOfMemoryError e) {
                e.printStackTrace();
                return bitmap;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return bitmap;
    }

    private static int getExifOrientation(String src) throws IOException {
        int orientation = 1;

        try {
            /**
             * if your are targeting only api level >= 5
             * ExifInterface exif = new ExifInterface(src);
             * orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, 1);
             */
            if (Build.VERSION.SDK_INT >= 5) {
                Class<?> exifClass = Class.forName("android.media.ExifInterface");
                Constructor<?> exifConstructor = exifClass.getConstructor(new Class[] { String.class });
                Object exifInstance = exifConstructor.newInstance(new Object[] { src });
                Method getAttributeInt = exifClass.getMethod("getAttributeInt", new Class[] { String.class, int.class });
                Field tagOrientationField = exifClass.getField("TAG_ORIENTATION");
                String tagOrientation = (String) tagOrientationField.get(null);
                orientation = (Integer) getAttributeInt.invoke(exifInstance, new Object[] { tagOrientation, 1});
            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SecurityException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        }

        return orientation;
    }

    public void delete(File file) throws IOException {
        if (file.isDirectory()) {
            //directory is empty?
            if (file.list().length == 0) {
                Toast.makeText(MainActivity.this, "La directory è vuota: " + file.getAbsolutePath(), Toast.LENGTH_LONG).show();
            } else {
                //list all the directory contents
                String files[] = file.list();
                for (String temp : files) {
                    //construct the file structure
                    File fileDelete = new File(file, temp);
                    //recursive delete
                    delete(fileDelete);
                }
                //check the directory again
                if (file.list().length == 0) {
                    Toast.makeText(MainActivity.this, "Tutti i file sono stati cancellati: " + file.getAbsolutePath(), Toast.LENGTH_SHORT).show();
                }
            }
        } else {
            //if file, then delete it
            file.delete();
            Toast.makeText(MainActivity.this, "File cancellato: " + file.getAbsolutePath(), Toast.LENGTH_LONG).show();
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 1: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    //utente ha detto ok => quindi posso fare quello che dovevo fare
                    try {
                        start_camera();
                        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
                            cameraSource.start(cameraView.getHolder());
                            return;
                        }
                    } catch (CameraAccessException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {
                    //stock
                    Toast.makeText(getApplicationContext(), "PERMISSION DENIED", Toast.LENGTH_LONG).show();
                    this.finish();
                }
            }
        }
    }

    @Override
    protected void onDestroy() {
        Process.killProcess(Process.myPid());
        super.onDestroy();
        cameraSource.release();
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if(id == R.id.action_credentials){
            Intent toy = new Intent(MainActivity.this, Main2Activity.class);
            startActivity(toy);
        }
        if (id == R.id.action_exit) {
            deleteCache(MainActivity.this);
            this.finishAffinity();
            finishAndRemoveTask();
        }
        return super.onOptionsItemSelected(item);
    }

    public String start_camera() throws CameraAccessException {
        cameraManager = (CameraManager) this.getSystemService(Context.CAMERA_SERVICE);
        String frontCameraId = getFrontFacingCameraId(cameraManager);
        //return cameraManager.getCameraIdList();
        return frontCameraId;
    }

    public String getFrontFacingCameraId(CameraManager cManager) throws CameraAccessException {
        for(final String cameraId : cManager.getCameraIdList()){
            CameraCharacteristics characteristics = cManager.getCameraCharacteristics(cameraId);
            int cOrientation = characteristics.get(CameraCharacteristics.LENS_FACING);
            if(cOrientation == CameraCharacteristics.LENS_FACING_BACK)
                return cameraId;
        }
        return null;
    }

    public static void deleteCache(Context context) {
        try {
            File dir = context.getCacheDir();
            deleteDir(dir);
        } catch (Exception e) {}
    }

    public static boolean deleteDir(File dir) {
        if (dir != null && dir.isDirectory()) {
            String[] children = dir.list();
            for (int i = 0; i < children.length; i++) {
                boolean success = deleteDir(new File(dir, children[i]));
                if (!success) {
                    return false;
                }
            }
            return dir.delete();
        } else if(dir!= null && dir.isFile()) {
            return dir.delete();
        } else {
            return false;
        }
    }

    // Upload captured image online on server function.
    public void ImageUploadToServerFunction(final String idSM, final String idRichiesta, final File directory){

        // Converting bitmap image to jpeg format, so by default image will upload in jpeg format.


        class AsyncTaskUploadClass extends AsyncTask<Void,Void,String> {

            @Override
            protected void onPreExecute() {

                super.onPreExecute();

                // Showing progress dialog at image upload time.
                progressDialog = ProgressDialog.show(MainActivity.this,"Images Uploading","Please Wait",false,false);
            }

            @Override
            protected void onPostExecute(String string1) {

                super.onPostExecute(string1);

                // Dismiss the progress dialog after done uploading.
                progressDialog.dismiss();

                // Printing uploading success message coming from server on android app.
                Toast.makeText(MainActivity.this,string1,Toast.LENGTH_LONG).show();
                // Setting image as transparent after done uploading.
                //ImageViewHolder.setImageResource(android.R.color.transparent);
                if (!string1.startsWith("Errore")) {
                    deleteDir(directory);
                }
            }

            @Override
            protected String doInBackground(Void... params) {
                String FinalData = "";
                for(File fileEntry : directory.listFiles()) {
                    String imageName = fileEntry.getName();
                    String fullPath = path + "/" + fileEntry.getName();
                    ImageProcessClass imageProcessClass = new ImageProcessClass();

                    HashMap<String,String> HashMapParams = new HashMap<String,String>();

                    HashMapParams.put("imageName", imageName);
                    final String ConvertImage = getImageBase64(fullPath);
                    HashMapParams.put("image", ConvertImage);

                    HashMapParams.put("idSM", idSM);

                    HashMapParams.put("idRichiesta", idRichiesta);

                    String responseData = imageProcessClass.ImageHttpRequest(ImageUploadPathOnSever, HashMapParams);
                    boolean Valid = true;
                    String errori = "";
                    if (!responseData.startsWith("Error:")) {
                        try {
                            JSONObject jObject = new JSONObject(responseData);
                            Valid = jObject.getBoolean("Valid");
                            JSONArray errorMessages = jObject.getJSONArray("ErrorMessages");
                            if (errorMessages != null && errorMessages.length() > 0) {
                                for (int i = 0; i < errorMessages.length(); i++) {
                                    try {
                                        // Pulling items from the array
                                        errori += errorMessages.getString(i) + System.getProperty("line.separator");
                                    } catch (JSONException e) {
                                        // Oops
                                    }
                                }
                            }
                        } catch (JSONException ex) {
                            ex.printStackTrace();
                        }
                    }
                    else {
                        Valid = false;
                        errori = responseData;
                    }
                    FinalData += Valid ? imageName + " inviata\n" : "Errore: " + errori;
                }

                return FinalData;
            }
        }
        AsyncTaskUploadClass AsyncTaskUploadClassOBJ = new AsyncTaskUploadClass();

        AsyncTaskUploadClassOBJ.execute();
    }

    public class ImageProcessClass{

        public String ImageHttpRequest(String requestURL,HashMap<String, String> PData) {
            String username = sharedPref.getString(TEXT_DATA_KEY2, null);
            String password = sharedPref.getString(TEXT_DATA_KEY3, null);
            StringBuilder stringBuilder = new StringBuilder();

            try {

                URL url;
                HttpURLConnection httpURLConnectionObject ;
                OutputStream OutPutStream;
                BufferedWriter bufferedWriterObject ;
                BufferedReader bufferedReaderObject ;
                int RC ;

                url = new URL(requestURL);

                httpURLConnectionObject = (HttpURLConnection) url.openConnection();

                String userCredentials = username+":"+password;
                String basicAuth = "Basic " + Base64.encodeToString(userCredentials.getBytes(), Base64.DEFAULT);
                httpURLConnectionObject.setRequestProperty ("Authorization", basicAuth);

                httpURLConnectionObject.setReadTimeout(19000);

                httpURLConnectionObject.setConnectTimeout(19000);

                httpURLConnectionObject.setRequestMethod("POST");

                httpURLConnectionObject.setDoInput(true);

                httpURLConnectionObject.setDoOutput(true);

                OutPutStream = httpURLConnectionObject.getOutputStream();

                bufferedWriterObject = new BufferedWriter(

                        new OutputStreamWriter(OutPutStream, "UTF-8"));

                bufferedWriterObject.write(bufferedWriterDataFN(PData));

                bufferedWriterObject.flush();

                bufferedWriterObject.close();

                OutPutStream.close();

                RC = httpURLConnectionObject.getResponseCode();

                if (RC == HttpsURLConnection.HTTP_OK) {

                    bufferedReaderObject = new BufferedReader(new InputStreamReader(httpURLConnectionObject.getInputStream()));

                    stringBuilder = new StringBuilder();

                    String RC2;

                    while ((RC2 = bufferedReaderObject.readLine()) != null){

                        stringBuilder.append(RC2);
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
                stringBuilder.append("Error: " + e.getMessage());
            }
            return stringBuilder.toString();
        }

        private String bufferedWriterDataFN(HashMap<String, String> HashMapParams) throws UnsupportedEncodingException {

            StringBuilder stringBuilderObject;

            stringBuilderObject = new StringBuilder();
            boolean check = true;
            for (Map.Entry<String, String> KEY : HashMapParams.entrySet()) {

                if (check)

                    check = false;
                else
                    stringBuilderObject.append("&");

                stringBuilderObject.append(URLEncoder.encode(KEY.getKey(), "UTF-8"));

                stringBuilderObject.append("=");

                stringBuilderObject.append(URLEncoder.encode(KEY.getValue(), "UTF-8"));
            }

            return stringBuilderObject.toString();
        }

    }
}
