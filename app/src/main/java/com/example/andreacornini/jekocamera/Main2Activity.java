package com.example.andreacornini.jekocamera;

import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.util.TypedValue;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;

public class Main2Activity extends AppCompatActivity {
    public static EditText username;
    public static EditText password;
    public SharedPreferences sharedPref;
    public static final String MY_PREFERENCES = "JekoCamera";
    public static final String TEXT_DATA_KEY2 = "textData2";
    public static final String TEXT_DATA_KEY3 = "textData3";
    public AlertDialog.Builder alertDialogBuilder;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        username = (EditText) findViewById(R.id.editTextUserName);
        password = (EditText) findViewById(R.id.editTextPassword);
        sharedPref = getSharedPreferences(MY_PREFERENCES, Context.MODE_PRIVATE);
        Button connect = (Button) findViewById(R.id.btnConnect);
        connect.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                savePreferencesData();
            }
        });
        CheckBox chkMostraPwd = (CheckBox) findViewById(R.id.chkMostraPwd);
        chkMostraPwd.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(!isChecked) {
                    password.setInputType(129);
                    password.setTypeface(Typeface.MONOSPACE);
                } else {
                    password.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
                    password.setTypeface(Typeface.MONOSPACE);
                }
            }
        });
        username.setText(sharedPref.getString(TEXT_DATA_KEY2, null));
        password.setText(sharedPref.getString(TEXT_DATA_KEY3, null));
        alertDialogBuilder = new AlertDialog.Builder(Main2Activity.this);
    }

    public void savePreferencesData() {
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(TEXT_DATA_KEY2, username.getText().toString());
        editor.putString(TEXT_DATA_KEY3, password.getText().toString());
        editor.commit();
        alertDialogBuilder
                .setMessage("Successfully saved credentials!")
                .setCancelable(false)
                .setPositiveButton("OK",new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
        TextView textView = (TextView) alertDialog.findViewById(android.R.id.message);
        textView.setTypeface(Typeface.MONOSPACE);
        textView.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 18);
    }
}
